- 👋 Hi, I’m @bbuske, a freelancer for content creation and web development
- 👀 I’m interested in programming, web & software development
- 🌱 I’m working with HTML5, CSS3, C / C++, Java, Scala & Kotlin
- 💞️ I’m looking to collaborate on code projects
- 📫 How to reach me: GitHub, GitLab, Email, [Website](https://www.buske-it.com)

## Services
I know HTML, CSS, JavaScript, Java, Kotlin, Scala, C and C++. I am an Arch Linux User and Linux system administrator. In addition, we provide support for Linux systems, such as technical and installation assistance. Additional services include copywriting, content creation, SEO and translations. I am native / fluent in English, German and Spanish. 

I work with market leading business partners for crypto currency and other crypto related projects as well as for complex platform and web development projects. If you are interested, in our services or wish to receive more information, please get [in touch](https://www.buske-it.com).
<!---
bbuske/bbuske is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
